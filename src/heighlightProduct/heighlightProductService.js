const heighlightModel = require('./heighlightProductModel');

/**
 * find from the system
 */
module.exports.findProducts = (condition) => {
    return new Promise((resolve, reject) => {
        heighlightModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        heighlightModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        heighlightModel.findOneAndDelete({ id: id },
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (body) => {
    return new Promise((resolve, reject) => {
        heighlightModel.findOne({ id: body.id }, function (err, productData) {
            console.log(productData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (productData == undefined || productData == null) {
                const product = new heighlightModel();
                product.id = body.id;
                product.is_new_arrivals = body.is_new_arrivals;
                product.is_on_sale = body.is_on_sale;
                product.is_best_rated = body.is_best_rated;
                product.is_featured = body.is_featured;

                product.save((error, productCb) => {
                    (error) ? reject(error) : resolve(productCb);
                });
            } else {
                reject("Product ID already exists");
            }
        })
    })
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        heighlightModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            heighlightModel.findOne({ id: id }, function (err1, productData) {
            console.log(id);
            console.log(updateObject);
            if (err1) {
                reject(err1);
            }
            else if (productData != undefined || productData != null) {
                heighlightModel.update({id  : id}, {$set: updateObject}).exec((err, data) => {
                    err ? reject(err) : resolve(data);
                });
            }else{
                reject("heighlight Product ID not found");
            }

        });
        } catch (error) {
            reject(error);
        }
    })
};

/**
 * find  from the system by id
 */
module.exports.validate = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await trainService.findTrainIdAccordingToLocation(data);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })
};
