const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const heighlightProductSchema = new Schema(
    {
        id: {
            type: Number,
            required: true,
            unique: true
        },
        is_new_arrivals: {
            type: Boolean,
            required: true
        },
        is_on_sale: {
            type: Boolean,
            required: true
        },
        is_best_rated: {
            type: Boolean,
            required: true
        },
        is_featured: {
            type: Boolean,
            required: true
        },
    }, { timestamps: true }
);
// productSchema.index({ product: "2dsphere" });
module.exports = mongoose.model('heighlight', heighlightProductSchema);
