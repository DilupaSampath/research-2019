const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  id: joi.number().required(),
  _id: joi.allow(),
  createdAt: joi.allow(),
  updatedAt: joi.allow(),
  __v : joi.allow(),
  currentRating: joi.number().required(),
  name: joi.string().required(),
  sale: joi.boolean().required(),
  date: joi.string().required(),
  categories: joi.object().required(),
  ratings: joi.object().required(),
  description: joi.string().required(),
  image_urls: joi.array().items(joi.string()),
  image_refs: joi.array().items(joi.string()),
  reduction: joi.number().required(),
  actual_price: joi.number().required(),
  original_price: joi.number().required(),
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  id: joi.allow(),
  _id: joi.allow(),
  createdAt: joi.allow(),
  updatedAt: joi.allow(),
  __v : joi.allow(),
  currentRating: joi.number(),
  name: joi.string(),
  sale: joi.boolean(),
  date: joi.string(),
  categories: joi.object(),
  ratings: joi.object(),
  description: joi.string(),
  image_urls: joi.array().items(joi.string()),
  image_refs: joi.array().items(joi.string()),
  reduction: joi.number(),
  actual_price: joi.number(),
  original_price: joi.number(),
});

module.exports.updateHighlight = joi.object().keys({
  is_new_arrivals: joi.boolean().required(),
  is_on_sale: joi.boolean().required(),
  is_best_rated: joi.boolean().required(),
  is_featured: joi.boolean().required(),
});