const productModel = require('./productModel');
const heighlightService = require('../heighlightProduct/heighlightProductService');
const heighlightProductModel= require('../heighlightProduct/heighlightProductModel');
/**
 * find from the system
 */
module.exports.findProducts = (condition) => {
    return new Promise((resolve, reject) => {
        productModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};

function findProductsLocal (condition) {
    return new Promise((resolve, reject) => {
        productModel.findOne(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        productModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};
module.exports.getHighLight = async (condition) => {
    return new Promise( async (resolve, reject) => {
        var resProducts = [];
        heighlightProductModel.find(condition).exec(async (err, products) => {
            try {
                if(!err){
                    if(products.length>0){
                        console.log('heighlightProductModel products **');
                        console.log(products);
                        products.forEach(async (highLightElement, index)=>{
                            let mainProduct = await findProductsLocal({id:highLightElement.id});
                            resProducts.push(mainProduct);
                            if(index >= products.length-1){
                                console.log('heighlightProductModel res products **');
                                console.log(resProducts);
                                resolve(resProducts);
                            }
                        });
                        
                    }else{
                        // noroducts
                        resolve(resProducts);
                    }
                }else{
                    //err
                    reject(err) 
                }
            } catch (error) {
                reject(error) 
            }
            // err ? reject(err) : resolve(product);
           
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        productModel.findOneAndDelete({ id: id },
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        productModel.findOne({ id: body.id },async  function (err, productData) {
            console.log(productData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (productData == undefined || productData == null) {
                const product = new productModel();
                product.id = body.id;
                product.name = body.name;
                product.date = body.date;
                product.reduction = body.reduction;
                product.sale = body.sale;
                product.currentRating = body.currentRating;
                product.categories = body.categories;
                product.description = body.description;
                product.image_urls = body.image_urls;
                product.actual_price = body.actual_price;
                product.original_price = body.original_price;

                product.save(async (error, productCb) => {
                    if(error){
                        reject(error);
                    }else{
                        const heighlightProduct = new heighlightProductModel();
                        heighlightProduct.id = product.id;
                        heighlightProduct.is_new_arrivals = false;
                        heighlightProduct.is_on_sale = false;
                        heighlightProduct.is_best_rated = false;
                        heighlightProduct.is_featured = false;
                        try {
                            console.log('in highlight');
                            let items = await heighlightService.create(heighlightProduct.id);
                            resolve(productCb);
                        } catch (error1) {
                            console.log('in highlight original delete');
                            productModel.findOneAndDelete({ id: heighlightProduct },
                                async (err, product1) => {
                                    if (err)
                                        reject(err);
                                    else if (product1 != null || product1 != undefined) {
                                        console.log('in highlight original deletedddddd');
                                        resolve("cannot create highlight and original product was successfully deleted");
                                    } else
                                        reject("cannot create highlight and original product ID is Invalid");
                                });
                            reject(error1);
                        }


                    }

                });
            } else {
                reject("Product ID already exists");
            }
        })
    })
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        locationModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            productModel.findOne({ id: id }, function (err1, productData) {
            console.log(id);
            console.log(updateObject);
            if (err1) {
                reject(err1);
            }
            else if (productData != undefined || productData != null) {
                productModel.update({id  : id}, {$set: updateObject}).exec((err, data) => {
                    err ? reject(err) : resolve(data);
                });
            }else{
                reject("Product ID not found");
            }

        });
        } catch (error) {
            
        }
    })
};

/**
 * find  from the system by id
 */
module.exports.validate = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await trainService.findTrainIdAccordingToLocation(data);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })
};
