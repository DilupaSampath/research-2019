const userModel = require('./userModel');

/**
 * create new user
 * @param {*} body 
 */
module.exports.createUser = (body) => {
    console.log("In Service");
    console.log(body);
    console.log("In Service");
    return new Promise((resolve, reject) => {
        userModel.findOne({ email: body.email }, function (err, userData) {
            console.log(userData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (userData == undefined || userData == null) {
                const user = new userModel();
                user.email = body.email;
                user.setPassword(body.password);
                user.save((error, user) => {
                    (error) ? reject(error) : resolve(user);
                })
            } else {
                reject("User already exists");
            }
        })
    })
}
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        userModel.find(condition).exec((err, user) => {
            err ? reject(err) : resolve(user);
        });
    })
};

/**

/**
 * login user 
 * @param {*} usser 
 */
module.exports.loginUser = (user) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(user);
            userModel.findOne({ email: user.email }, function (err, userData) {
                if (err) {
                    reject(err);
                }
                if (userData != undefined || userData != null) {
                    let status = userData
                        .validPassword(user.password);
                    (status) ?
                        resolve({status:true,id:userData._id}) :
                        reject("Invalid user name or password");
                } else {
                    reject("Invalid user name or password");
                }
            })
        } catch (error) {
            console.log(error);
            reject('' + error);
        }
    })
}

// /**
//  * find and update from the system by id
//  */
// module.exports.findByIdAndUpdateScores =async  (id, data) => {
//     return new Promise(async (resolve, reject) => {
//         if(data.score){
//             userModel.findOne({ _id: id })
//             .exec((err, responce) => {
//                if(!err){
//                    console.log(responce);
//                    console.log(responce.score.total);
//                    let newScore =  0;

//                    if(responce.score.total != undefined){
//                     newScore =  responce.score.total + data.score;
//                     if(data.date){

//                     }
//                    }else{
//                     newScore = data.score;
//                    }
                    

//                     userModel.findByIdAndUpdate(id, {score:{total:newScore,date: new Date()}}, { new: false })
//                     .exec((err, result) => {
//                         if (err)
//                             reject(err)
//                         else if (result == null || result == undefined)
//                             reject("Somthing went wrong!");
//                         else
//                             resolve(result);
//                     });
//                }else{
//                 reject("Invalid id");
//                }
//             });
          
//         }else{
//             reject("user score not found");
//         }

//     })
// };