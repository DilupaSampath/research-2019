// import validator class
const joi = require('joi');

// user scorce Schema and validations to be done
module.exports.scores = joi.object().keys({
    score: joi.number().required(),
     id: joi.string().alphanum().min(24).max(24).required(),
});
// user reporting Schema and validations to be done
module.exports.reporting = joi.object().keys({
    free_sheets: joi.boolean().required(),
    is_last_station_on_time: joi.boolean().required(),
    crowd: joi.boolean().required(),
     id: joi.string().alphanum().min(24).max(24).required(),
});
module.exports.login = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    password: joi.required(),
});
// user registration Schema and validations to be done
module.exports.newUser = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    role: joi
        .string()
        .required(),
    password: joi.string()
        .min(6)
        .required(),
})