const locationModel = require('./locationModel');
const trainService = require('../train/trainService');
const googleApiService = require('../../services/google-api-service');

/**
 * find from the system
 */
module.exports.findLocations = (condition) => {
    return new Promise((resolve, reject) => {
        locationModel.find(condition).exec((err, location) => {
        
            err ? reject(err) : resolve(location);
        });
    })
};

module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        locationModel.find(condition).exec((err, location) => {
            err ? reject(err) : resolve(location);
        });
    })
};
/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        locationModel.findOneAndDelete({ _id: id },
            async (err, location) => {
                if (err)
                    reject(err);
                else if (location != null || location != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            // console.log({"lat":data.location.coordinates[0],"lang":data.location.coordinates[1]});
           let locationRequesData = await googleApiService.getRealLocationData({"lat":data.location.coordinates[0],"lang":data.location.coordinates[1]});
             
           if(!locationRequesData.status){
            console.log("cannot find the location");
            reject("cannot find the location..!");
        }else{
            console.log(locationRequesData);
            let location = new locationModel();
                    location.name = locationRequesData.data.name;
                    data.location.coordinates = [locationRequesData.data.cordinates.lng,locationRequesData.data.cordinates.lat]
                    location.location = data.location;
                    location.save(async (error, data) => {
                        if (error) {
                            console.log(error);
                            if(error.name=='MongoError' && error.errmsg.includes("duplicate key error")){
                                reject("Location already exist..!");
                            }else{
                                reject(error);
                            }

                        } else {
                            resolve(data);
                        }
                    })
        }
        
        } catch (error) {
            reject('' + error)
        }
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        locationModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate =async  (id, data) => {
    return new Promise(async (resolve, reject) => {
        if(data.location){
            let locationRequesData = await googleApiService.getRealLocationData({"lat":data.location.coordinates[0],"lang":data.location.coordinates[1]});
            if(!locationRequesData.status){
                console.log("cannot find the location");
                reject("cannot find the location..!");
            }else{
                    data.name = locationRequesData.data.name;
                    data.location.coordinates = [locationRequesData.data.cordinates.lng,locationRequesData.data.cordinates.lat]
                    data.location = data.location;
                    locationModel.findByIdAndUpdate(id, data, { new: true })
                    .exec((err, result) => {
                        if (err)
                        
                            reject(err)
                        else if (result == null || result == undefined)
                            reject("Invalid id");
                        else
                            resolve(result);
                    });
            }
        }else{
            console.log("noloc");
            locationModel.findByIdAndUpdate(id, data, { new: true })
                    .exec((err, result) => {
                        if (err)
                        
                            reject(err)
                        else if (result == null || result == undefined)
                            reject("Invalid id");
                        else
                            resolve(result);
                    });
        }

    })
};

/**
 * find  from the system by id
 */
module.exports.validate =async (data) => {
    return new Promise( async (resolve, reject) => {
      try {
        let result = await trainService.findTrainIdAccordingToLocation(data);
        resolve(result);
      } catch (error) {
          reject(error);
      }
       
      
      

    })
};
