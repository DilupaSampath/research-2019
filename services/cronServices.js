
const dateService = require('./dateService');
const timeTableService = require('../src/time-table/timeTableService')
const CronJob = require('cron').CronJob;
const socketService = require('./socketService');

//realtime train cron service
function runLiveSchedulesCron() {
    new CronJob('*/4 * * * * *', async () => {
        socketService.emitLiveScheduleData();
    }, null, true, '');
}
//user chat report cron service
// function runChatReportCron() {
//     new CronJob('*/2 * * * * *', async () => {
//         socketService.getchatReports();
//     }, null, true, '');
// }
//main valve cron service
async function runfindAndUpdateTrainRunningStatusCron() {
    new CronJob('*/4 * * * * *', async () => {
        // console.log("rning");
       var t= await timeTableService.findAndUpdateTrainRunningStatus();
    }, null, true, '');
}

//main valve cron service
function runReadMainValveDataCron() {
    new CronJob('*/2 * * * * *', async () => {
        // console.log("rning read");

    }, null, true, '');
}

//rack cron service
function runCheckRacksStatusCron() {
    new CronJob('*/20 * * * * *', async () => {
    }, null, true, '');
}

//Run Server Time Cron cron service
function runServerTimeCron() {
    new CronJob('* * * * * *', async () => {
    }, null, true, '')
}
//export functions
module.exports = {
    runfindAndUpdateTrainRunningStatusCron,
    runLiveSchedulesCron
};