'use strict';
// Import Express
const express = require('express');
// user router
const router = express.Router();
// Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json({ limit: '50mb' }));
router.use(bodyParser.json());

// import  controllers
const userController = require('../src/user/userController');
const productController = require('../src/product/productController');
const productHighlightController = require('../src/heighlightProduct/heighlightProductController');


// import validator Schemas
const userSchema = require('../src/user/userSchema');
const produtSchema = require('../src/product/productSchema');

// import Validator class
const validator = require('../services/validator');

//user routes
router.route('/user/getAll')
    .get(userController.getAll);
router.route('/user/new')
    .post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/user/login')
    .post(validator.validateBody(userSchema.login), userController.login);

//product routes
router.route('/product/new')
    .post(validator.validateBody(produtSchema.create), productController.create);
router.route('/product/getAll')
    .get(productController.getAll);
router.route('/product/highlight/filter')
    .get(productController.getHighlightFilter);
router.route('/product/remove/:id')
    .delete(productController.remove);
router.route('/product/update/:id')
    .patch(validator.validateBody(produtSchema.update), productController.update);
router.route('/product/highlight/update/:id')
    .patch(validator.validateBody(produtSchema.updateHighlight), productHighlightController.update);
router.route('/product/highlight/remove/:id')
    .delete(productHighlightController.remove);
module.exports = router;
